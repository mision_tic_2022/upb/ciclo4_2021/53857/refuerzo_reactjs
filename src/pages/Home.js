import React from "react";
import { Outlet } from "react-router";
import { Navbar, Nav, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { AlbumProvider } from "../context/AlbumContext";
import {BlogProvider} from "../context/BlogContext";

const Home = () => {
  return (
    <div>
      <Navbar bg="dark" expand="lg" variant="dark">
        <Container>
          <Navbar.Brand href="#blog">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/" href="#blog">
                Blog
              </Nav.Link>
              <Nav.Link as={Link} to="album" href="#album">
                Album
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      {/******Llamar/utilizar/mostrar/visualizar.... rutas anidadas*******/}
      <BlogProvider>
        <AlbumProvider>
          <Outlet />
        </AlbumProvider>
      </BlogProvider>
    </div>
  );
};

export default Home;
