import React, {useContext} from 'react'
import Photos from './Photos'
import AlbumContext from "../context/AlbumContext";
import "./Components.css";

const Album = () => {

    const {photos} = useContext(AlbumContext);

    return (
        <div>
            <h2>Album</h2>
            <div className="container">
            {photos.map(e=><Photos key={e.id} title={e.title} url={e.url}/>)}
            </div>
        </div>
    )
}

export default Album
