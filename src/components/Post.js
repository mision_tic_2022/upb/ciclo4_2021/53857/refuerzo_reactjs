import React from "react";
import { Card } from "react-bootstrap";

const Post = ({title, body}) => {
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Body>{body}</Card.Body>
      </Card.Body>
    </Card>
  );
};

export default Post;
