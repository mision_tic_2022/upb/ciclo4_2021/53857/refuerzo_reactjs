import {createContext, useEffect, useState} from "react";
import { apiPhotos } from "./Api";

const AlbumContext = createContext();

const AlbumProvider = ({children})=>{

    const [photos, setPhotos] = useState([]);

    useEffect(()=>{
        handlePhotos();
    }, []);
    

    const handlePhotos = ()=>{
        fetch(apiPhotos).then(async (resp)=>{
            let json = await resp.json();
            setPhotos(json);
        }).catch(error=>{
            console.error(error);
        }).finally();
    }

    const data ={photos, handlePhotos}
    return <AlbumContext.Provider value={data}>{children}</AlbumContext.Provider>
}

export {AlbumProvider};
export default AlbumContext;