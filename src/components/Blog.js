import React, { useContext } from 'react'
import BlogContext from "../context/BlogContext";
import Post from './Post';

const Blog = () => {

    const {posts} = useContext(BlogContext);

    return (
        <div>
            <h2>Blog</h2>
            {posts.map(e=><Post key={e.id} title={e.title} body={e.body}/>)}
        </div>
    )
}

export default Blog;
