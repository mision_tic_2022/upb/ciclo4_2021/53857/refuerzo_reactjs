import React from 'react'
import { Route, Routes } from 'react-router';
import Album from '../components/Album';
import Blog from '../components/Blog';
import Home from '../pages/Home';

const Routers = () => {
    return (
        <Routes>
            <Route path="/" element={<Home/>}>
                <Route index element={<Blog/>}/>
                <Route path="album" element={<Album/>}/>
            </Route>
        </Routes>
    )
}

export default Routers;
