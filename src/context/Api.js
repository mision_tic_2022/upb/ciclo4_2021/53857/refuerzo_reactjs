const root = "https://jsonplaceholder.typicode.com";
const apiPhotos = `${root}/photos`;
const apiPosts = `${root}/posts`;

export {apiPhotos, apiPosts};