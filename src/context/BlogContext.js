import { createContext, useEffect, useState } from "react";
import { apiPosts } from "./Api";

const BlogContext = createContext();

const BlogProvider = ({ children }) => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    handlePosts();
  }, []);

  const handlePosts = () => {
    fetch(apiPosts)
      .then(async (resp) => {
        let json = await resp.json();
        setPosts(json);
      })
      .catch((error) => {})
      .finally();
  };

  const data = { posts, handlePosts };

  return <BlogContext.Provider value={data}>{children}</BlogContext.Provider>;
};

export { BlogProvider };
export default BlogContext;
